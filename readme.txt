Run the stock game firstly and switch to DX9 in the settings menu. Then quit the game.

You can also make sure by looking for "Render DX9" in the config file "C:/Users/${USER_ID}/Documents/Ubisoft/Tom Clancy's Ghost Recon Future Soldier/default.cfg".

Finally, copy the patch files to the game installation directory and override any existing files.
